//
//  BluetoothClass.swift
//  MCHPBluetooth
//
//  Created by Jawer on 11/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//
//  Original by created by Ricky Johnson on 10/29/18.
//  Copyright © 2018 Ricky Johnson. All rights reserved.


import Foundation
import CoreBluetooth

var bluetoothConnection: BluetoothClass!

protocol BluetoothClassDelegate{
    /* Here you can define custom functions that will be available for any object of the BluetoothClassType*/
}

final class BluetoothClass: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var delegate: BluetoothClassDelegate!
    
    //bluetooth Variables
    var manager: CBCentralManager!
    var connectedPeripheral: CBPeripheral!
    var peripherals: [(peripheral: CBPeripheral, RSSI: Float)] = []
    
    //if you use a module other than the RN4871/70 change the service, tx, and RX uuid to the correct uuids for your module
    var myisscServiceUUID = CBUUID(string: "49535343-FE7D-4AE5-8FA9-9FAFD205E455")
    var myisscTxUUID      = CBUUID(string: "49535343-8841-43F4-A8D4-ECBE34729BB3")
    var myisscRxUUID      = CBUUID(string: "49535343-1E4D-4BD9-BA61-23C647249616")
    
    //this will be set to writeWitoutResponse or writeWithResponse depending on your modules protocol
    var writeCharacteristic:CBCharacteristic?
    
    //variables used in iOS code
    var receivedString:String!              //string recieved from module
    var receivedFrame = [UInt8](repeating: 0, count:3) //raw recieved from module
    var initialized = false                 //has corebluetooth been initialized?
    var isconnectedToPeripheral = false     //are we connected to a peripheral
    
    //initialize core bluetooth
    init(delegate: BluetoothClassDelegate){
        super.init()
        //change later
        self.delegate = delegate
        manager = CBCentralManager(delegate:self, queue: nil)
        receivedString = ""
    }
    
    // Start scanning for peripherals
    func startScan() {
        guard manager.state == .poweredOn else { return }
        
        // start scanning for peripherals with correct service UUID
        manager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }

    /* Called when BLE turns on or off*/
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            central.scanForPeripherals(withServices: nil, options: nil)
        } else {
            print("bluetooth not available")
        }
    }
    
    /* Called any time a peripheral is discovered
     discovered a peripheral with the defined uuid so add it to the list of peripherals available, reorganize by RSSI, dont allow for duplicates*/
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        // check whether it is a duplicate
        for exisiting in peripherals {
            if exisiting.peripheral.identifier == peripheral.identifier { return }
        }
        if peripheral.name == nil {return}
        // add to the array, next sort & reload
        let theRSSI = RSSI.floatValue
        peripherals.append((peripheral: peripheral, RSSI: theRSSI))
        peripherals.sort { $0.RSSI < $1.RSSI }
        NotificationCenter.default.post(name: .foundPeripheral, object: nil)
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        peripheral.discoverServices([myisscServiceUUID])
    }
    
    /* Called any time a service has been discovered for a given peripheral
     we have discovered a peripheral with a matching service uuid*/
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            if service.uuid == myisscServiceUUID {
                peripheral.discoverCharacteristics([myisscTxUUID, myisscRxUUID], for: service )
            }
        }
    }
    
    /*Called any time we discover a characteristic for a service(s)
     we now know that we have found a peripheral with matching service, tx and rx uuid. Therefore we will go ahead and set peripheral connected to true*/
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        connectedPeripheral = peripheral
        
        for characteristic in service.characteristics! {
            
            if (service.uuid == myisscServiceUUID) {
                if (characteristic.uuid == myisscTxUUID) {
                    print("discovered TX characteristics")
                    peripheral.setNotifyValue(true, for: characteristic )
                    writeCharacteristic = characteristic
                }
                
                if (characteristic.uuid == myisscRxUUID) {
                    print("discovered RX characteristics")
                    peripheral.setNotifyValue(true, for: characteristic )
                }
            }
            
        }
        isconnectedToPeripheral = true
    }
    
    var count = 0
    
    /*this function will be called any time a string is recieved from your partnered bluetooth module*/
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        let data = characteristic.value
        guard data != nil else { return } // Like a light assert
        
        let data_size = data?.count ?? 0
        
        // Check if there is any data (redundant)
        guard data_size >= 4 else {
            print("data_size < 4, \(data_size)")
            return
        }
        // Check if data messages are in packets of 4 bytes
        guard data_size%4 == 0 else {
           print("data_size%4 != 0, \(data_size)")
           return
       }
        
        var frame:LMFrame = LMFrame()
        frame.value = 0
        frame.option = 0
        frame.mode_track = 0
        
        var pointer = 0
        data!.forEach { data_byte in
            if pointer == 0 {
                // Value
                frame.value = data_byte
            } else if pointer == 1 {
                // Option
                frame.option = data_byte
            } else if pointer == 2 {
                // Mode + track
                frame.mode_track = data_byte
            } else if pointer == 3 {
                // Mode + track (again)
                print(frame)
                if data_byte != frame.mode_track {
                    print("Se ha vuelto loco")
                } else {
                    // Parse received frame, and update local status
                    receivedString = LMParser.parse(frame)
                    // Notify change
                    NotificationCenter.default.post(name: .stringReceived, object: nil)
                    // Reset frame
                    frame.value = 0
                    frame.option = 0
                    frame.mode_track = 0
                }
            }
            
            pointer = (pointer+1)%4
        }
        
    }
}
