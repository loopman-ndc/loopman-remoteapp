//
//  ScannerViewController.swift
//  MCHPBluetooth
//
//  Created by Jawer on 11/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//
//  Original by created by Ricky Johnson on 10/29/18.
//  Copyright © 2018 Ricky Johnson. All rights reserved.

import UIKit
import CoreBluetooth

final class ScannerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BluetoothClassDelegate {
    

//MARK: IBOutlets
    
    @IBOutlet weak var tryAgainButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
//MARK: Variables
    
    /// The peripherals that have been discovered
    var peripherals: [(peripheral: CBPeripheral, RSSI: Float)] = []
    
    /// The peripheral the user has selected
    var selectedPeripheral: CBPeripheral?
    
    var connectionTimer: Timer!     // this is used to periodically try to connect to a given peripheral.
    var connectionAttempts: Int = 0     //this is to count how many times we've tried to connect

//MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tryAgainButton.isEnabled = false
        
        /*we want to be notified if a peripheral is found so that we can add it to the table*/
        NotificationCenter.default.addObserver(self, selector: #selector(peripheralFound(notification:)), name: .foundPeripheral, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*function that gets called if we get a peripheral found notification*/
    @objc func peripheralFound(notification: NSNotification){
        tableView.reloadData()
    }

    
//MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bluetoothConnection.peripherals.count
    }
    
    /*setting up a table cell which will display the name of the peripheral*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // return a cell with the peripheral name as text in the label
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let label = cell.viewWithTag(1) as! UILabel?
        let str = bluetoothConnection.peripherals[(indexPath as NSIndexPath).row].peripheral.name
        if (str == "PmodBLE-842F") {
            label?.text = "LoopMAN"
        } else {
            label?.text = str
        }
        return cell
    }
    
    
//MARK: UITableViewDelegate
    
    /*if we select a given peripheral stop scanning for new ones. set selectedPeripheral to what we selected.  try to connect to selected peripheral*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        // the user has selected a peripheral, so stop scanning and proceed to the next view
        bluetoothConnection.manager.stopScan()
        selectedPeripheral = bluetoothConnection.peripherals[(indexPath as NSIndexPath).row].peripheral
        bluetoothConnection.manager.connect(selectedPeripheral!, options: nil)
        connectionTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkIfConnectSuccessful), userInfo: nil, repeats: false)
    }
    
    /*if isconnectedToPeripheral never got set to true and we tried to connect to the peripheral more than 5 times, let the user know that we couldn't connect
    
     else, stop triggering the connection timer and let the user know that we successfully connected, also go back to the home*/
    
    @objc func checkIfConnectSuccessful(){
        //could not connect after 5 seconds of trying
        if (!bluetoothConnection.isconnectedToPeripheral && connectionAttempts > 5) {
             connectionTimer.invalidate()
            
             let alert = UIAlertController(title: "Connection Time Out", message: "Could not connect to this peripheral", preferredStyle: .alert)
             //remove later
             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
           
             present(alert, animated: true)
        } else {
            connectionTimer.invalidate()
            
            let alert = UIAlertController(title: "Connection Success", message: "Close this notification to go back to home screen and begin sending messages.", preferredStyle: .alert)
             //remove later
             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
             alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
             bluetoothConnection.isconnectedToPeripheral = true
                self.performSegue(withIdentifier: "returnedFromScanner", sender: Any?.self)
             }))
             present(alert, animated: true)
        
        }
    }
    

//MARK: IBActions
    
    /* take user back to home page */
    
    @IBAction func cancelPressed(_ sender: Any) {
        bluetoothConnection.manager.stopScan()
        performSegue(withIdentifier: "returnedFromScanner", sender: Any?.self)
    }
    

    /* empty all of the periherals found, turn off try again so that users cant keep pressing it, start scanning for peripherals,reload the table data */
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        // empty array an start again
       bluetoothConnection.peripherals = []
        
        tryAgainButton.isEnabled = false
        bluetoothConnection.manager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        
        // clear table load for some time while scan completes
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            self.tableView.reloadData()
        })
    }
    
}
