//
//  ParserMan.swift
//  MCHPBluetoth
//
//  Created by Jawer on 11/03/2020.
//

import Foundation

var LMParser : LMParserClass! = LMParserClass()

final class LMParserClass {
    init(){}
    
    func parse(_ frame: LMFrame) -> String {
        var parse_info : String = ""
        
        print(frame)
        
        let mode   : UInt8 = (frame.mode_track & 0b11111000) >> 3
        let track  : UInt8 = frame.mode_track & 0b00000111
        let option : UInt8 = frame.option
        let value  : UInt8 = frame.value
        
        if mode == BTF_mode_tempo {
            parse_info += "\nMode tempo:"
            let ticks : UInt32 = UInt32(((UInt16(option) << 8) | UInt16(value))) << 12; // 12 by loopman design
            LMStatus.st_tempo = UInt16(Int(Float(CLOCK)/Float(Int(ticks)) * 60.0));
            parse_info += "\n  Tempo = \(Int(LMStatus.st_tempo))"
            parse_info += "\n  Ticks = \(Int(ticks))"
            
        } else if mode == BTF_mode_master {
            parse_info += "\nMode master, output \(Int(track)):"
            switch option {
            case BTF_opt_set_vol:
                LMStatus.st_master_vol[Int(track)] = value
                parse_info += "\n  Volume = \(Int(value))"
                break
            case BTF_opt_set_comp:
                LMStatus.st_master_comp[Int(track)] = value
                parse_info += "\n  Compression = \(Int(value))"
                break
            case BTF_opt_outlevel:
                LMStatus.st_master_outlevel[Int(track)] = 0xf & value
                LMStatus.st_master_clip[Int(track)] = 0x1 & (value >> 7)
                parse_info += "\n  Clipped = \(Int(LMStatus.st_master_clip[Int(track)]))"
                parse_info += "\n  Outlevel = \(Int(LMStatus.st_master_outlevel[Int(track)]))"
                break
            default:
                print("unknown")
            }
            
        } else if mode == BTF_mode_track {
            parse_info += "\nMode track, track status \(Int(track)):"
            switch option {
            case BTF_opt_track_status:
                var i = 0
                while i < BTF_TS_COUNT {
                    // Iterate over value to set new received status
                    let bit_val : UInt8 = ((frame.value >> i) & 1)
                    
                    switch i {
                    case BTF_TS_rec:
                        LMStatus.st_track_rec[Int(track)] = bit_val
                        parse_info += "\n  Rec = \(Int(bit_val))"
                        break
                    case BTF_TS_play:
                        LMStatus.st_track_play[Int(track)] = bit_val
                        parse_info += "\n  Play = \(Int(bit_val))"
                        break
                    case BTF_TS_mute:
                        LMStatus.st_track_mute[Int(track)] = bit_val
                        parse_info += "\n  Mute = \(Int(bit_val))"
                        break
                    case BTF_TS_solo:
                        LMStatus.st_track_solo[Int(track)] = bit_val
                        parse_info += "\n  Solo = \(Int(bit_val))"
                        break
                    case BTF_TS_sync:
                        LMStatus.st_track_sync[Int(track)] = bit_val
                        parse_info += "\n  Sync = \(Int(bit_val))"
                        break
                    case BTF_TS_auto_rec:
                        LMStatus.st_track_auto_rec[Int(track)] = bit_val
                        parse_info += "\n  Autorec = \(Int(bit_val))"
                        break
                    default:
                        print("aun no implementado")
                    }

                    i += 1
                }
                break
            case BTF_opt_beat:
                LMStatus.st_track_beat[Int(track)] = value
                parse_info += "\n  Beat = \(Int(value))"
                break
            case BTF_opt_set_looplen:
                LMStatus.st_track_looplen[Int(track)] = value
                parse_info += "\n  Loop length = \(Int(value))"
                break
            case BTF_opt_set_vol:
                LMStatus.st_track_vol[Int(track)] = value
                parse_info += "\n  Volume = \(Int(value))"
                break
            case BTF_opt_set_odub:
                LMStatus.st_track_odub[Int(track)] = value
                parse_info += "\n  Overdub = \(Int(value))"
                break
            case BTF_opt_input_sel:
                LMStatus.st_track_input_sel[Int(track)][0] = value & 0x1
                LMStatus.st_track_input_sel[Int(track)][1] = (value & 0x2) >> 1
                LMStatus.st_track_input_sel[Int(track)][2] = (value & 0x4) >> 2
                LMStatus.st_track_input_sel[Int(track)][3] = (value & 0x8) >> 3
                parse_info += "\n  Input selection = \(Int(value))"
                break
            case BTF_opt_output_sel:
                LMStatus.st_track_output_sel[Int(track)][0] = value & 0x1
                LMStatus.st_track_output_sel[Int(track)][1] = (value & 0x2) >> 1
                parse_info += "\n  Output selection = \(Int(value))"
                break
            case BTF_opt_outlevel:
                LMStatus.st_track_outlevel[Int(track)] = value
                parse_info += "\n  Outputlevel = \(Int(value))"
                break
            default:
                print("unknown")
            }
        } else if mode == BTF_mode_fx {
            parse_info += "\nMode FX, "
            // Option holds which fx is modified
            
            // Check if new enqueueing happens
            if (LMStatus.st_fx_queued[Int(track)][Int(option)] == 0 &&
                (0b00000001 & (value >> 1)) == 1) {
                LMStatus.st_fx_queue_pos[Int(track)][Int(option)] = LMStatus.st_fx_queue_count[Int(track)]
                LMStatus.st_fx_queue[Int(track)][Int(LMStatus.st_fx_queue_count[Int(track)])] = option
                LMStatus.st_fx_queue_count[Int(track)] += 1
                
            } else if (LMStatus.st_fx_queued[Int(track)][Int(option)] == 1 &&
                      (0b00000001 & (value >> 1)) == 0) {
                // When any fx of a track has been dequeued, it means that queue has been reset
                LMStatus.st_fx_queue_count[Int(track)] = 0;
                LMStatus.st_fx_queue_pos[Int(track)] = [UInt8](repeating:0, count:FX_NO)
                LMStatus.st_fx_queue[Int(track)] = [UInt8](repeating:0, count:FX_NO)
            }
            
            LMStatus.st_fx_on[Int(track)][Int(option)]     = 0b00000001 & value
            LMStatus.st_fx_queued[Int(track)][Int(option)] = 0b00000001 & (value >> 1)
            LMStatus.st_fx_value[Int(track)][Int(option)]  = 0b00001111 & (value >> 4)
            
            parse_info += "\n  Track   = \(Int(track))"
            parse_info += "\n  Fx      = \(Int(option))"
            parse_info += "\n  Queued  = \(Int(LMStatus.st_fx_queued[Int(track)][Int(option)]))"
            parse_info += "\n  Pos     = \(Int(LMStatus.st_fx_queue_pos[Int(track)][Int(option)]))"
            parse_info += "\n  Enabled = \(Int(LMStatus.st_fx_on[Int(track)][Int(option)]))"
            parse_info += "\n  Value   = \(Int(LMStatus.st_fx_value[Int(track)][Int(option)]))"
            
        } else if mode == BTF_reset {
            // Reset all to zero
            LMStatus.reset()
            parse_info += "\nReset received"
        }
        
        NotificationCenter.default.post(name: .itemChanged, object: nil)
        
        print(parse_info)
        return parse_info
    }

}
