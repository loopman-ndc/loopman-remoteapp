//
//  SystemParameters.swift
//  MCHPBluetoth
//
//  Created by Jawer on 13/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//

import Foundation

let CLOCK = 75000000

let FX_NO = 9
let SYS_INPUTS = 4
let SYS_OUTPUTS = 2

let MAX_TEMPO = 400
let MIN_TEMPO = 45

let MAX_TICKS = Int((60.0/Float(MAX_TEMPO))*Float(CLOCK))
let MIN_TICKS = Int((60.0/Float(MIN_TEMPO))*Float(CLOCK))

// MARK: FRAME CONSTANTS
let BTF_reset            : UInt8 = 0
// MODE
let BTF_mode_track       : UInt8 = 0b00001
let BTF_mode_fx          : UInt8 = 0b00010
let BTF_mode_master      : UInt8 = 0b00011
let BTF_mode_forbidden   : UInt8 = 0b00100 // Dont use
let BTF_mode_tempo       : UInt8 = 0b00101
// OPTION
let BTF_opt_rec          : UInt8 = 0b00000001
let BTF_opt_play         : UInt8 = 0b00000010
let BTF_opt_mute         : UInt8 = 0b00000011
let BTF_opt_solo         : UInt8 = 0b00000100
let BTF_opt_clear        : UInt8 = 0b00000101
let BTF_opt_sync         : UInt8 = 0b00000110
let BTF_opt_auto_rec     : UInt8 = 0b00000111
let BTF_opt_track_status : UInt8 = 0b00001000
let BTF_opt_beat         : UInt8 = 0b00001001
let BTF_opt_inc_looplen  : UInt8 = 0b00001010
let BTF_opt_dec_looplen  : UInt8 = 0b00001011
let BTF_opt_set_looplen  : UInt8 = 0b00001100
let BTF_opt_inc_vol      : UInt8 = 0b00001101
let BTF_opt_dec_vol      : UInt8 = 0b00001110
let BTF_opt_set_vol      : UInt8 = 0b00001111
let BTF_opt_inc_odub     : UInt8 = 0b00010000
let BTF_opt_dec_odub     : UInt8 = 0b00010001
let BTF_opt_set_odub     : UInt8 = 0b00010010
let BTF_opt_input_sel    : UInt8 = 0b00010011
let BTF_opt_output_sel   : UInt8 = 0b00010100
let BTF_opt_outlevel     : UInt8 = 0b00010101
let BTF_opt_inc_comp     : UInt8 = 0b00010110
let BTF_opt_dec_comp     : UInt8 = 0b00010111
let BTF_opt_set_comp     : UInt8 = 0b00011000
// FX OPTION -> fx id,
// FX VALUE -> value & xx & enqueue & enable
let BTF_fxopt_clear      : UInt8 = 0b00000001
let BTF_fxopt_enqueue    : UInt8 = 0b00000010
let BTF_fxopt_enable     : UInt8 = 0b00000011
let BTF_fxopt_set_value  : UInt8 = 0b10000000
// TRACK STATUS INDEX
let BTF_TS_COUNT         = 6 // Total number of track parameters
let BTF_TS_rec           = 0
let BTF_TS_play          = 1
let BTF_TS_mute          = 2
let BTF_TS_solo          = 3
let BTF_TS_sync          = 4
let BTF_TS_auto_rec      = 5

// MARK: STRUCTURES
struct LMFrame {
    var mode_track: UInt8 = 0
    var option: UInt8 = 0
    var value: UInt8 = 0
    
    init() {
        mode_track = 0
        option = 0
        value = 0
    }
    
    func text() -> String {
        var strMtr = String(mode_track, radix: 2)
        var strOpt = String(option, radix: 2)
        var strVal = String(value, radix: 2)
        
        for _ in 0..<(8 - strMtr.count) {
          strMtr = "0" + strMtr
        }
        for _ in 0..<(8 - strOpt.count) {
          strOpt = "0" + strOpt
        }
        for _ in 0..<(8 - strVal.count) {
          strVal = "0" + strVal
        }
        
        let str:String = strMtr + " " + strOpt + " " + strVal
        
        return str
    }
    
    func size() -> Int {
        return 3
    }
    
    func serial() -> [UInt8] {
        let rawData:[UInt8] = [mode_track, option, value]
        
        return rawData
    }
}

//MARK: FX
let fxDict:[Int:String] = [0:"Delay",
                           1:"Overdrive",
                           2:"Jamón",
                           3:"Fuzz",
                           4:"Compresor",
                           5:"Tremolo",
                           6:"Flanger",
                           7:"HiCut",
                           8:"LowCut"]
