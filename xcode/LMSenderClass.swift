//
//  LMSenderClass.swift
//  MCHPBluetoth
//
//  Created by Jawer on 13/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//

import Foundation
import CoreBluetooth

var LMSender : LMSenderClass! = LMSenderClass()

class LMSenderClass {
    
    init(){}
    
    func sendFrame(_ frame:LMFrame!){
        if bluetoothConnection.isconnectedToPeripheral  == true {
            var rawData = frame.serial()
            let frameSize = frame.size()
            
            let data:Data = Data(bytes: &rawData, count: frameSize)

            if let peri:CBPeripheral = bluetoothConnection.connectedPeripheral {
                peri.writeValue(data, for: bluetoothConnection.writeCharacteristic!, type: .withoutResponse)

                print("Sent \(frameSize) bytes:", frame.text())
            } else {
                print("Bt failure")
            }
        } else {
            print("LoopMAN not connected")
        }
    }

}
