//
//  LMStatusClass.swift
//  MCHPBluetoth
//
//  Created by Jawer on 11/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//

import Foundation

var LMStatus : LMStatusClass! = LMStatusClass()

class LMStatusClass {
    var st_tempo            : UInt16
    
    var st_master_vol       : [UInt8]
    var st_master_comp      : [UInt8]
    var st_master_outlevel  : [UInt8]
    var st_master_clip      : [UInt8]
    
    var st_track_rec        : [UInt8]
    var st_track_play       : [UInt8]
    var st_track_mute       : [UInt8]
    var st_track_solo       : [UInt8]
    var st_track_sync       : [UInt8]
    var st_track_auto_rec   : [UInt8]
    var st_track_beat       : [UInt8]
    var st_track_looplen    : [UInt8]
    var st_track_vol        : [UInt8]
    var st_track_odub       : [UInt8]
    var st_track_input_sel  : [[UInt8]]
    var st_track_output_sel : [[UInt8]]
    var st_track_outlevel   : [UInt8]
    
    var st_fx_on            : [[UInt8]]
    var st_fx_queued        : [[UInt8]]
    var st_fx_value         : [[UInt8]]
    var st_fx_queue_count   : [UInt8]
    var st_fx_queue_pos     : [[UInt8]] // For each effect, in which position is it
    var st_fx_queue         : [[UInt8]] // For each position, to which effect is it assigned
    
    init(){
        self.st_tempo            = 120
        
        self.st_master_vol       = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_comp      = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_outlevel  = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_clip      = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        
        self.st_track_rec        = [UInt8](repeating: 0, count:8)
        self.st_track_play       = [UInt8](repeating: 0, count:8)
        self.st_track_mute       = [UInt8](repeating: 0, count:8)
        self.st_track_solo       = [UInt8](repeating: 0, count:8)
        self.st_track_sync       = [UInt8](repeating: 1, count:8)
        self.st_track_auto_rec   = [UInt8](repeating: 0, count:8)
        self.st_track_beat       = [UInt8](repeating: 0, count:8)
        self.st_track_looplen    = [UInt8](repeating: 31, count:8)
        self.st_track_vol        = [UInt8](repeating: 15, count:8)
        self.st_track_odub       = [UInt8](repeating: 15, count:8)
        self.st_track_input_sel  = [[UInt8]](repeating: [UInt8](repeating:0, count:SYS_INPUTS), count:8)
        self.st_track_output_sel = [[UInt8]](repeating: [UInt8](repeating:0, count:SYS_OUTPUTS), count:8)
        self.st_track_outlevel   = [UInt8](repeating: 0, count:8)
        
        self.st_fx_on            = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queued        = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_value         = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queue_count   = [UInt8](repeating: 0, count:8)
        self.st_fx_queue_pos     = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queue         = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
    }
    
    func reset(){
        self.st_tempo            = 120
        
        self.st_master_vol       = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_comp      = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_outlevel  = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        self.st_master_clip      = [UInt8](repeating: 0, count:SYS_OUTPUTS)
        
        self.st_track_rec        = [UInt8](repeating: 0, count:8)
        self.st_track_play       = [UInt8](repeating: 0, count:8)
        self.st_track_mute       = [UInt8](repeating: 0, count:8)
        self.st_track_solo       = [UInt8](repeating: 0, count:8)
        self.st_track_sync       = [UInt8](repeating: 1, count:8)
        self.st_track_auto_rec   = [UInt8](repeating: 0, count:8)
        self.st_track_beat       = [UInt8](repeating: 0, count:8)
        self.st_track_looplen    = [UInt8](repeating: 31, count:8)
        self.st_track_vol        = [UInt8](repeating: 15, count:8)
        self.st_track_odub       = [UInt8](repeating: 15, count:8)
        self.st_track_input_sel  = [[UInt8]](repeating: [UInt8](repeating:0, count:SYS_INPUTS), count:8)
        self.st_track_output_sel = [[UInt8]](repeating: [UInt8](repeating:0, count:SYS_OUTPUTS), count:8)
        self.st_track_outlevel   = [UInt8](repeating: 0, count:8)
        
        self.st_fx_on            = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queued        = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_value         = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queue_count   = [UInt8](repeating: 0, count:8)
        self.st_fx_queue_pos     = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
        self.st_fx_queue         = [[UInt8]](repeating: [UInt8](repeating:0, count:FX_NO), count:8)
    }
}
