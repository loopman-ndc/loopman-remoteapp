//
//  BluetoothHomepageViewController.swift
//  MCHPBluetooth
//
//  Created by Jawer on 11/03/2020.
//  Copyright © 2020 nuevedecopas. All rights reserved.
//


import UIKit
import CoreBluetooth

enum controlType {
    case play
    case mute
    case solo
    case rec
    case autorec
    case sync
    case clr
    case vol
    case looplen_sl
    case looplen_st
    case master_vol
    case master_comp
    case input_0
    case input_1
    case output_0
    case output_1
    case odub
    case tempo_sl
    case fx_clr
    case input_2
    case input_3
}

enum fxControlType {
    case non
    case enable
    case slider
}

struct controlId{
    let controlDict:[controlType: UInt16] = [controlType.play:0,
                                            controlType.mute:1,
                                            controlType.solo:2,
                                            controlType.rec:3,
                                            controlType.autorec:4,
                                            controlType.sync:5,
                                            controlType.clr:6,
                                            controlType.vol:7,
                                            controlType.looplen_sl:8,
                                            controlType.looplen_st:9,
                                            controlType.master_vol:10,
                                            controlType.master_comp:11,
                                            controlType.input_0:12,
                                            controlType.input_1:13,
                                            controlType.output_0:14,
                                            controlType.output_1:15,
                                            controlType.odub:16,
                                            controlType.tempo_sl:17,
                                            controlType.fx_clr:18,
                                            controlType.input_2:19,
                                            controlType.input_3:20]

    let controlArray:[controlType] = [controlType.play,
                                      controlType.mute,
                                      controlType.solo,
                                      controlType.rec,
                                      controlType.autorec,
                                      controlType.sync,
                                      controlType.clr,
                                      controlType.vol,
                                      controlType.looplen_sl,
                                      controlType.looplen_st,
                                      controlType.master_vol,
                                      controlType.master_comp,
                                      controlType.input_0,
                                      controlType.input_1,
                                      controlType.output_0,
                                      controlType.output_1,
                                      controlType.odub,
                                      controlType.tempo_sl,
                                      controlType.fx_clr,
                                      controlType.input_2,
                                      controlType.input_3]
    
    var track:UInt16
    var type:controlType
    
    init(track:UInt16, type:controlType){
        self.track = track
        self.type = type
    }
    
    init(encodedInt:Int){
        self.track = UInt16(encodedInt) & 0xff
        let arrayIndex = (UInt16(encodedInt) >> 8) & 0xff
        self.type = controlArray[Int(arrayIndex)]
    }
    
    func encodeInt() -> Int{
        let byteType:UInt16 = controlDict[type] ?? 0
        
        let encodedId:UInt16 = ((track & 0xff) | ((byteType & 0xff) << 8))
        
        return Int(encodedId)
    }
}

struct fxControlId{
    // Must not be 0, 1, 2 or 3 as it may collide with table
    let fxControlDict:[fxControlType: UInt16] = [fxControlType.enable:4,
                                                 fxControlType.slider:5]

    let fxControlArray:[fxControlType] = [fxControlType.non,
                                          fxControlType.non,
                                          fxControlType.non,
                                          fxControlType.non,
                                          fxControlType.enable,
                                          fxControlType.slider]
    
    var id:UInt16
    var type:fxControlType
    
    init(id:UInt16, type:fxControlType){
        self.id = id
        self.type = type
    }
    
    init(encodedInt:Int){
        self.id = UInt16(encodedInt) & 0xff
        let arrayIndex = (UInt16(encodedInt) >> 8) & 0xff
        self.type = fxControlArray[Int(arrayIndex)]
    }
    
    func encodeInt() -> Int{
        let byteType:UInt16 = fxControlDict[type] ?? 0
        
        let encodedId:UInt16 = ((id & 0xff) | ((byteType & 0xff) << 8))
        
        return Int(encodedId)
    }
}

@IBDesignable
open class VerticalUISlider: UISlider {
    
    override open func draw(_ rect: CGRect) {
        self.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
    }
}

@IBDesignable
open class VerticalUIProgressView: UIProgressView {
    
    override open func draw(_ rect: CGRect) {
        self.transform = CGAffineTransform(scaleX: 8.0, y: 1.0).rotated(by: CGFloat(-Double.pi/2))
    }
}

@IBDesignable
open class SmallUIStepper: UIStepper {
    
    override open func draw(_ rect: CGRect) {
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    }
}

@IBDesignable
open class FxUISlider: UISlider {
    
    var innerID : Int = 0
}

@IBDesignable
open class FxUISwitch: UISwitch {
    
    var innerID : Int = 0
}

final class BluetoothHomepageViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, BluetoothClassDelegate{
   
//MARK: IBOutlets
    
    @IBOutlet var mainText: UITextView!

    @IBOutlet var navBar: UINavigationItem!
    
    @IBOutlet var m_slider_vol: [VerticalUISlider]!
    @IBOutlet var m_bar_outlevel: [VerticalUIProgressView]!
    @IBOutlet var m_slider_comp: [VerticalUISlider]!
    @IBOutlet var m_label_clip: [UILabel]!
    
    var m_outlevel_progress = [Progress](repeating: Progress(totalUnitCount: 15), count: 8)

    @IBOutlet var t_but_on: [UIButton]!
    @IBOutlet var t_but_rec: [UIButton]!
    @IBOutlet var t_but_mute: [UIButton]!
    @IBOutlet var t_but_solo: [UIButton]!
    @IBOutlet var t_but_autorec: [UIButton]!
    @IBOutlet var t_but_sync: [UIButton]!
    @IBOutlet var t_slider_odub: [UISlider]!
    @IBOutlet var t_but_clr: [UIButton]!
    
    @IBOutlet var t_slider_vol: [VerticalUISlider]!
    @IBOutlet var t_bar_outlevel: [VerticalUIProgressView]!
    @IBOutlet var t_slider_looplen: [VerticalUISlider]!
    @IBOutlet var t_bar_beat: [VerticalUIProgressView]!
    @IBOutlet var t_stepper_looplen: [SmallUIStepper]!
    @IBOutlet var t_label_beat: [UILabel]!
    
    var outlevel_progress = [Progress](repeating: Progress(totalUnitCount: 15), count: 8)
    var beat_progress = [Progress](repeating: Progress(totalUnitCount: 31), count: 64)

    @IBOutlet var switch_input_0: [UISwitch]!
    @IBOutlet var switch_input_1: [UISwitch]!
    @IBOutlet var switch_input_2: [UISwitch]!
    @IBOutlet var switch_input_3: [UISwitch]!
    @IBOutlet var switch_output_0: [UISwitch]!
    @IBOutlet var switch_output_1: [UISwitch]!
    
    @IBOutlet var slider_tempo: UISlider!
    @IBOutlet var label_tempo: UILabel!
    
    @IBOutlet weak var fxTrackSelect: UISegmentedControl!
    @IBOutlet weak var fxTrackPedalboard: UITableView!
    @IBOutlet weak var fxList: UITableView!
    @IBOutlet weak var fxClr: UIButton!
    
    var receivedStrings = ""

//MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bluetoothConnection = BluetoothClass(delegate: self)
    
        mainText?.text = receivedStrings
        
        var id:controlId
        id = controlId(track:UInt16(0), type:controlType.tempo_sl)
        slider_tempo.tag = id.encodeInt()
        slider_tempo.setValue(calculateTempoSliderVal(), animated: true)
        slider_tempo.isContinuous = true
        slider_tempo.addTarget(self, action: #selector(actionControl), for: .valueChanged)
        
        label_tempo.text = "\(LMStatus.st_tempo)"

        var i = 0
        while (i < SYS_OUTPUTS){
            // Volume & outlevel
            id = controlId(track:UInt16(i), type:controlType.master_vol)
            m_slider_vol[i].tag = id.encodeInt()
            m_slider_vol[i].setValue(0.0, animated: true)
            m_slider_vol[i].isContinuous = true
            m_slider_vol[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            m_outlevel_progress[i] = Progress(totalUnitCount: 15)
            m_bar_outlevel[i].observedProgress = m_outlevel_progress[i]
            
            // Compressor & clip
            id = controlId(track:UInt16(i), type:controlType.master_comp)
            m_slider_comp[i].tag = id.encodeInt()
            m_slider_comp[i].setValue(0.0, animated: true)
            m_slider_comp[i].isContinuous = true
            m_slider_comp[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            m_label_clip[i].text = "CLIP"
            m_label_clip[i].backgroundColor = UIColor.init(red: 0.334, green: 0.203, blue: 0.194, alpha: 1);
            
            i += 1;
        }
        
        i = 0
        while (i < 8){
            // Play
            t_but_on[i].setTitle("\(i+1)", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.play)
            t_but_on[i].tag = id.encodeInt()
            t_but_on[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Mute
            t_but_mute[i].setTitle("M", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.mute)
            t_but_mute[i].tag = id.encodeInt()
            t_but_mute[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Solo
            t_but_solo[i].setTitle("S", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.solo)
            t_but_solo[i].tag = id.encodeInt()
            t_but_solo[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Rec
            t_but_rec[i].setTitle("R", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.rec)
            t_but_rec[i].tag = id.encodeInt()
            t_but_rec[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Autorec
            t_but_autorec[i].setTitle("AUTO", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.autorec)
            t_but_autorec[i].tag = id.encodeInt()
            t_but_autorec[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Sync
            t_but_sync[i].setTitle("SYNC", for: UIControl.State.normal)
            t_but_sync[i].backgroundColor = UIColor.lightGray
            id = controlId(track:UInt16(i), type:controlType.sync)
            t_but_sync[i].tag = id.encodeInt()
            t_but_sync[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Odub
            id = controlId(track:UInt16(i), type:controlType.odub)
            t_slider_odub[i].tag = id.encodeInt()
            t_slider_odub[i].setValue(1.0, animated: true)
            t_slider_odub[i].isContinuous = true
            t_slider_odub[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            // Clr
            t_but_clr[i].setTitle("CLR", for: UIControl.State.normal)
            id = controlId(track:UInt16(i), type:controlType.clr)
            t_but_clr[i].tag = id.encodeInt()
            t_but_clr[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            // Volume & outlevel
            id = controlId(track:UInt16(i), type:controlType.vol)
            t_slider_vol[i].tag = id.encodeInt()
            t_slider_vol[i].setValue(1.0, animated: true)
            t_slider_vol[i].isContinuous = true
            t_slider_vol[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            outlevel_progress[i] = Progress(totalUnitCount: 15)
            t_bar_outlevel[i].observedProgress = outlevel_progress[i]
            
            // Beat & looplen
            id = controlId(track:UInt16(i), type:controlType.looplen_sl)
            t_slider_looplen[i].tag = id.encodeInt()
            t_slider_looplen[i].setValue(1.0, animated: true)
            t_slider_looplen[i].isContinuous = true
            t_slider_looplen[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.looplen_st)
            t_stepper_looplen[i].tag = id.encodeInt()
            t_stepper_looplen[i].maximumValue = 31
            t_stepper_looplen[i].minimumValue = 0
            t_stepper_looplen[i].stepValue = 1
            t_stepper_looplen[i].value = 31
            t_stepper_looplen[i].addTarget(self, action: #selector(actionControl), for: .touchUpInside)
            
            t_label_beat[i].text = "0/32"
            beat_progress[i] = Progress(totalUnitCount: 31)
            t_bar_beat[i].observedProgress = beat_progress[i]
            
            // IO Switches
            id = controlId(track:UInt16(i), type:controlType.input_0)
            switch_input_0[i].tag = id.encodeInt()
            switch_input_0[i].isOn = false
            switch_input_0[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.input_1)
            switch_input_1[i].tag = id.encodeInt()
            switch_input_1[i].isOn = false
            switch_input_1[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.input_2)
            switch_input_2[i].tag = id.encodeInt()
            switch_input_2[i].isOn = false
            switch_input_2[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.input_3)
            switch_input_3[i].tag = id.encodeInt()
            switch_input_3[i].isOn = false
            switch_input_3[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.output_0)
            switch_output_0[i].tag = id.encodeInt()
            switch_output_0[i].isOn = false
            switch_output_0[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            id = controlId(track:UInt16(i), type:controlType.output_1)
            switch_output_1[i].tag = id.encodeInt()
            switch_output_1[i].isOn = false
            switch_output_1[i].addTarget(self, action: #selector(actionControl), for: .valueChanged)
            
            i += 1
        }
        
        // Fx hot stuff lists
        fxTrackSelect.addTarget(self, action: #selector(refreshFxTable), for: .valueChanged)
        
        fxList.reloadData()
        
        // FX CLR
        fxClr.setTitle("FX CLR", for: UIControl.State.normal)
        id = controlId(track:UInt16(0), type:controlType.fx_clr)
        fxClr.tag = id.encodeInt()
        fxClr.addTarget(self, action: #selector(actionControl), for: .touchUpInside)
        
        DispatchQueue.main.async {
            if bluetoothConnection.initialized  == true {
                if(bluetoothConnection.manager.state  == .poweredOn){
                    self.navBar.title = "Connected"
                } else {
                    self.navBar.title = "Disconnected"
                }
            }
        }
        
        /*this creates a notification when a string comes in*/
        NotificationCenter.default.addObserver(self, selector: #selector(incomingString(notification:)), name: .stringReceived, object: nil)
        
        /*this creates a notification when a status change happens*/
        NotificationCenter.default.addObserver(self, selector: #selector(statusChange(notification:)), name: .itemChanged, object: nil)
    }
    
    /*function that gets executed when the received string notification happens*/
    @objc func incomingString(notification: NSNotification){
        DispatchQueue.main.async {
            self.receivedStrings = bluetoothConnection.receivedString!
            self.mainText.text = self.receivedStrings
            //self.parsedFrame.text = bluetoothConnection.receivedString
        }
    }
    
    /* Status changed in loopman */
    @objc func statusChange(notification: NSNotification){
        DispatchQueue.main.async {
            var red:Float = 0.0
            var green:Float = 0.0
            var blue:Float = 0.0
            
            // TEMPO
            if (!self.slider_tempo.isTracking) {
                self.slider_tempo.setValue(self.calculateTempoSliderVal(), animated: true)
            }
            
            self.label_tempo.text = "\(LMStatus.st_tempo)"
            
            var i = 0
            while (i < 8){
                if (i < SYS_OUTPUTS){
                    if (!self.m_slider_vol[i].isTracking) {
                        self.m_slider_vol[i].setValue(Float(Int(LMStatus.st_master_vol[i]))/15.0, animated: true)
                    }

                    self.m_outlevel_progress[i].completedUnitCount = Int64(LMStatus.st_master_outlevel[i])
                    red = (Float(Int(LMStatus.st_master_outlevel[i])))/15.0
                    green = 1.0 - red
                    blue = 0
                    self.m_bar_outlevel[i].progressTintColor = UIColor.init(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1)
                    
                    if (!self.m_slider_comp[i].isTracking) {
                        self.m_slider_comp[i].setValue(Float(Int(LMStatus.st_master_comp[i]))/15.0, animated: true)
                    }
                    
                    if LMStatus.st_master_clip[i] == 1 {
                        self.m_label_clip[i].backgroundColor = UIColor.red;
                    } else {
                        self.m_label_clip[i].backgroundColor = UIColor.init(red: 0.334, green: 0.203, blue: 0.194, alpha: 1);
                    }
                }
                
                if LMStatus.st_track_play[i] == 1 {
                    self.t_but_on[i].backgroundColor = UIColor.green
                } else {
                    self.t_but_on[i].backgroundColor = UIColor.init(red: 0.224, green: 0.306, blue: 0.246, alpha: 1)
                }
                
                if LMStatus.st_track_mute[i] == 1 {
                    self.t_but_mute[i].backgroundColor = UIColor.blue
                } else {
                    self.t_but_mute[i].backgroundColor = UIColor.init(red: 0.175, green: 0.296, blue: 0.508, alpha: 1)
                }
                
                if LMStatus.st_track_solo[i] == 1 {
                    self.t_but_solo[i].backgroundColor = UIColor.cyan
                } else {
                    self.t_but_solo[i].backgroundColor = UIColor.init(red: 0.065, green: 0.447, blue: 0.502, alpha: 1)
                }
                
                if LMStatus.st_track_rec[i] == 1 {
                    self.t_but_rec[i].backgroundColor = UIColor.red
                } else {
                    self.t_but_rec[i].backgroundColor = UIColor.init(red: 0.334, green: 0.203, blue: 0.194, alpha: 1)
                }
                
                if LMStatus.st_track_auto_rec[i] == 1 {
                    self.t_but_autorec[i].backgroundColor = UIColor.red
                } else {
                    self.t_but_autorec[i].backgroundColor = UIColor.init(red: 0.334, green: 0.203, blue: 0.194, alpha: 1)
                }
                
                if LMStatus.st_track_sync[i] == 1 {
                    self.t_but_sync[i].backgroundColor = UIColor.lightGray
                } else {
                    self.t_but_sync[i].backgroundColor = UIColor.init(red: 0.297, green: 0.297, blue: 0.293, alpha: 1)
                }
                
                if (!self.t_slider_odub[i].isTracking) {
                    self.t_slider_odub[i].setValue(Float(Int(LMStatus.st_track_odub[i]))/15.0, animated: true)
                }

                if (!self.t_slider_vol[i].isTracking) {
                    self.t_slider_vol[i].setValue(Float(Int(LMStatus.st_track_vol[i]))/15.0, animated: true)
                }

                self.outlevel_progress[i].completedUnitCount = Int64(LMStatus.st_track_outlevel[i])
                red = (Float(Int(LMStatus.st_track_outlevel[i])))/15.0
                green = 1.0 - red
                blue = 0
                self.t_bar_outlevel[i].progressTintColor = UIColor.init(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1)
                
                if (!self.t_slider_looplen[i].isTracking) {
                    self.t_slider_looplen[i].setValue(Float(Int(LMStatus.st_track_looplen[i]))/31.0, animated: true)
                }
                
                self.t_stepper_looplen[i].value = Double(Int(LMStatus.st_track_looplen[i]))
                        
                self.beat_progress[i].completedUnitCount = Int64(LMStatus.st_track_beat[i])

                self.t_label_beat[i].text = "\(Int(LMStatus.st_track_beat[i]))/\(Int(LMStatus.st_track_looplen[i])+1)"
                
                // IO switches
                if (!self.switch_input_0[i].isTouchInside) {
                    let disp : UInt8 = self.switch_input_0[i].isOn ? 1 : 0
                    let curr : UInt8  = LMStatus.st_track_input_sel[i][0] & 1
                    if ((disp ^ curr) > 0) {
                        self.switch_input_0[i].isOn  = (LMStatus.st_track_input_sel[i][0]  > 0) ? true : false;
                    }
                }
                
                if (!self.switch_input_1[i].isTouchInside) {
                    self.switch_input_1[i].isOn  = (LMStatus.st_track_input_sel[i][1]  > 0) ? true : false;
                }
                
                if (!self.switch_input_2[i].isTouchInside) {
                    self.switch_input_2[i].isOn  = (LMStatus.st_track_input_sel[i][2]  > 0) ? true : false;
                }
                
                if (!self.switch_input_3[i].isTouchInside) {
                    self.switch_input_3[i].isOn  = (LMStatus.st_track_input_sel[i][3]  > 0) ? true : false;
                }
                
                if (!self.switch_input_0[i].isTouchInside) {
                    self.switch_output_0[i].isOn = (LMStatus.st_track_output_sel[i][0] > 0) ? true : false;
                }
                
                if (!self.switch_input_0[i].isTouchInside) {
                    self.switch_output_1[i].isOn = (LMStatus.st_track_output_sel[i][1] > 0) ? true : false;
                }
                
                i += 1
            }
            
            // FX
            self.fxTrackPedalboard.reloadData()
        }
    }
  
//MARK: IBActions
    @IBAction func actionControl(_ sender: UIControl) {
        DispatchQueue.main.async {
            let id:controlId = controlId(encodedInt: sender.tag)
            
            print("Actioned \(id.type) in track: \(id.track)")
            
            var frame:LMFrame = LMFrame()
            let track:UInt8 = UInt8(id.track)
            
            switch id.type {
            case controlType.play:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_play
                frame.value = 0
                break
                
            case controlType.mute:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_mute
                frame.value = 0
                break
                    
            case controlType.solo:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_solo
                frame.value = 0
                break
        
            case controlType.rec:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_rec
                frame.value = 0
                break
                
            case controlType.autorec:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_auto_rec
                frame.value = 0
                break
            
            case controlType.sync:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_sync
                frame.value = 0
                break
            
            case controlType.clr:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_clear
                frame.value = 0
                break
                
            case controlType.vol:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_set_vol
                let slider = sender as! UISlider
                frame.value = UInt8(slider.value*15)
                break
                
            case controlType.looplen_sl:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_set_looplen
                let slider = sender as! UISlider
                frame.value = UInt8(slider.value*31)
                break
                
            case controlType.looplen_st:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_set_looplen
                let stepper = sender as! UIStepper
                frame.value = UInt8(stepper.value)
                break
                
            case controlType.master_vol:
                let mode:UInt8 = BTF_mode_master << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_set_vol
                let slider = sender as! UISlider
                frame.value = UInt8(slider.value*15)
                break
                
            case controlType.master_comp:
                let mode:UInt8 = BTF_mode_master << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_set_comp
                let slider = sender as! UISlider
                frame.value = UInt8(slider.value*15)
                break
            
            case controlType.input_0:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_input_sel
                let value = 1
                frame.value = UInt8(value)
                break
            
            case controlType.input_1:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_input_sel
                let value = 1<<1
                frame.value = UInt8(value)
                break
                
            case controlType.input_2:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_input_sel
                let value = 1<<2
                frame.value = UInt8(value)
                break
            
            case controlType.input_3:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_input_sel
                let value = 1<<3
                frame.value = UInt8(value)
                break
            
            case controlType.output_0:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_output_sel
                let value = 1
                frame.value = UInt8(value)
                break
            
            case controlType.output_1:
                let mode:UInt8 = BTF_mode_track << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_opt_output_sel
                let value = 1<<1
                frame.value = UInt8(value)
                break
            
           case controlType.odub:
               let mode:UInt8 = BTF_mode_track << 3
               
               frame.mode_track = track | mode
               frame.option = BTF_opt_set_odub
               let slider = sender as! UISlider
               frame.value = UInt8(slider.value*15)
               break
                
            case controlType.tempo_sl:
                let mode:UInt8 = BTF_mode_tempo << 3
                
                frame.mode_track = mode
                let slider = sender as! UISlider
                let ticks : UInt16 = self.ticksFromSlider(slider.value)
                frame.option = UInt8((ticks & 0xFF00) >> 8)
                frame.value = UInt8((ticks & 0xFF))
                break
                
            case controlType.fx_clr:
                let mode:UInt8 = BTF_mode_fx << 3
                
                let selectedTrack:UInt8 = UInt8(self.fxTrackSelect.selectedSegmentIndex)
                frame.mode_track = selectedTrack | mode
                frame.option = BTF_fxopt_clear
                frame.value = 0
                break
             
            default:
                break
            }

            LMSender.sendFrame(frame)
        }
    }
    
    @IBAction func fxActionControl(_ sender: UIControl) {
        DispatchQueue.main.async {
            var id:fxControlId
            
            if (sender.tag == 1) {
                let localSender = sender as! FxUISwitch
                id = fxControlId(encodedInt: localSender.innerID)
            } else if (sender.tag == 3) {
                let localSender = sender as! FxUISlider
                id = fxControlId(encodedInt: localSender.innerID)
            } else {
                return
            }
            
            print("Actioned \(id.type) in fx: \(id.id) of track: \(self.fxTrackSelect.selectedSegmentIndex)")
            
            var frame:LMFrame = LMFrame()
            let track:UInt8 = UInt8(self.fxTrackSelect.selectedSegmentIndex)
            frame.value = UInt8(id.id)
            
            switch id.type {
            case fxControlType.enable:
                let mode:UInt8 = BTF_mode_fx << 3
                
                frame.mode_track = track | mode
                frame.option = BTF_fxopt_enable
                break
            case fxControlType.slider:
                let mode:UInt8 = BTF_mode_fx << 3
                
                frame.mode_track = track | mode
                let slider = sender as! UISlider
                frame.option = BTF_fxopt_set_value | UInt8(slider.value*15)
                break
            default:
                break
            }
            
            LMSender.sendFrame(frame)
        }
    }
    
    @IBAction func refreshFxTable(_ sender: UIControl) {
        print("Selected track \(self.fxTrackSelect.selectedSegmentIndex)")
        
        self.fxTrackPedalboard.reloadData()
    }
    
//MARK: FX table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView.tag == 0) { // FxPedalboard
            return Int(LMStatus.st_fx_queue_count[self.fxTrackSelect.selectedSegmentIndex])
        } else { // Fx list
            return Int(FX_NO)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView.tag == 0) { // FxPedalboard
            let selectedTrack = self.fxTrackSelect.selectedSegmentIndex
            let currentFxId = LMStatus.st_fx_queue[selectedTrack][(indexPath as NSIndexPath).row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            
            var id = fxControlId(id:UInt16(currentFxId), type:fxControlType.enable)
            let sw = cell.viewWithTag(1) as! FxUISwitch?
            
            sw?.innerID = id.encodeInt()
            sw?.addTarget(self, action: #selector(fxActionControl), for: .valueChanged)
            sw?.setOn(LMStatus.st_fx_on[selectedTrack][Int(currentFxId)] == 1, animated: true)
            
            let label = cell.viewWithTag(2) as! UILabel?
            label?.text = fxDict[Int(currentFxId)]
            
            id = fxControlId(id:UInt16(currentFxId), type:fxControlType.slider)
            let slider = cell.viewWithTag(3) as! FxUISlider?
            
            slider?.innerID = id.encodeInt()
            slider?.addTarget(self, action: #selector(fxActionControl), for: .valueChanged)
            if (!(slider?.isTracking ?? true)) {
                slider?.setValue(Float(Int(LMStatus.st_fx_value[selectedTrack][Int(currentFxId)]))/15.0, animated: true)
            }
            
            return cell
        } else { // Fx list
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            let label = cell.viewWithTag(1) as! UILabel?
            label?.text = fxDict[(indexPath as NSIndexPath).row]
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var frame:LMFrame = LMFrame()
        let track:UInt8 = UInt8(self.fxTrackSelect.selectedSegmentIndex)
        frame.value = UInt8((indexPath as NSIndexPath).row)
        
        if (tableView.tag == 0) { // FxPedalboard
            
        } else { // Fx list
            let mode:UInt8 = BTF_mode_fx << 3
            
            frame.mode_track = track | mode
            frame.option = BTF_fxopt_enqueue
            LMSender.sendFrame(frame)
        }
    }
    
//MARK: Upper buttons

    @IBAction func resetPressed(_ sender: Any) {
        DispatchQueue.main.async {
            print("Sending reset request to LoopMAN...")
            
            let frame : LMFrame = LMFrame.init()
            LMSender.sendFrame(frame)
        }
    }
    
    @IBAction func connectPressed(_ sender: Any) {
        if bluetoothConnection.isconnectedToPeripheral == false {
            performSegue(withIdentifier: "ShowScanner", sender: self)
        } else {
            let alert = UIAlertController(title: "You Are Connected", message: "You are already connected to a device. If you ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
            
            alert.addAction(UIAlertAction(title: "Disconnect", style: .default, handler: { action in
                
            bluetoothConnection.manager.cancelPeripheralConnection(bluetoothConnection.connectedPeripheral)
                bluetoothConnection.isconnectedToPeripheral = false
                bluetoothConnection.connectedPeripheral = nil
                self.performSegue(withIdentifier: "ShowScanner", sender: Any?.self)
            }))
            present(alert, animated: true)
        }
        
    }
    
    func calculateTempoSliderVal() -> Float {
        let tempo_slider_val : Float = Float(Int(LMStatus.st_tempo) - MIN_TEMPO)/Float(MAX_TEMPO - MIN_TEMPO)
        return tempo_slider_val
    }
    
    func tempoFromSlider(_ val : Float) -> Int {
        let tempo : Int = Int(val*Float(MAX_TEMPO-MIN_TEMPO))+MIN_TEMPO
        return tempo
    }
    
    func ticksFromSlider(_ val : Float) -> UInt16 {
        let tempo : UInt16 = UInt16(Int(val*Float(MAX_TEMPO-MIN_TEMPO))+MIN_TEMPO)
        let ticks : UInt32 = UInt32(Int((60.0/Float(tempo))*Float(CLOCK)))
        return UInt16(ticks>>12)
    }

}

extension Notification.Name {
    static let stringReceived  = Notification.Name("stringReceived")
    static let itemChanged     = Notification.Name("itemChanged")
    static let foundPeripheral = Notification.Name("foundPeripheral")
}
